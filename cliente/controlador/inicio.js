//CONFIGURACION DE ANGULAR PARA EL MANEJO DE DATOS DEL LADO DEL CLIENTE
//SE DECLARA UN NUEVO MODULO
var app = angular.module('myApp', []);

//SE DECLARA UN NUEVO CONTROLADOR
app.controller('controlCtrl', ['$scope','ControlService', function($scope, ControlService) {
  //AQUI SE EMPIEZZAN A DECLARAR LAS FUNCIONES QUE VIENEN DEL HTML
    $scope.getData = function () {

      //SE CREA UN NUEVO SERVICIO (POR DONDE SE VA HACER LA PETICION AL SERVIDOR)
      ControlService.getData()
      .success(function(result){
        console.log(result,'aqui');
        //CUANDO EL SERVIDOR TERMINE VA A REGRESARNOS LOS DATOS DE LA PETICION
        $scope.getData = result.cat_precios;
        //console.log(result.cat_precios);
      });
    }

    $scope.show_msg_ok_agregar = false;
    $scope.btnAgregar = function (item) {
      console.log(item);
      if (item == undefined) {
        alert('Err Campos faltantes');
      } else {
        var obj = {nombre:item.nombre, precio: item.precio};
        if (obj.nombre ==  undefined) {
          $scope.show_alert_nombre = true;
        }

        if (obj.precio == undefined) {
          $scope.show_alert_precio = true;
        }

        if (obj.precio == undefined || obj.nombre == undefined) {
          alert('Faltan campos por completar');
        } else {
          console.log(item);
          $scope.getData.push(item);
          ControlService.agregarNuevo(item)
          .success(function(result){
            if (result.length == 0) {
              alert("Err");
            } else {
              $scope.show_form_agregar = false;
              $scope.show_btn_agregar = true;
              $scope.show_msg_ok_agregar = true;
            }

          });
        }
      }


    }

    $scope.show_btn_agregar = true;
    $scope.show_form_agregar = false;
    $scope.btnAgregarNuevo = function () {
      $scope.show_form_agregar = true;
      $scope.show_btn_agregar = false;
    }

    $scope.btnAgregarCancelar = function () {
      $scope.show_form_agregar = false;
      $scope.show_btn_agregar = true;
    }
}]);
//CREACION DEL SERVICIO
app.service('ControlService', ['$http', function($http) {
//DEFINICION DE LA URL BASE EN ESTE CASO SERA EL ARCHIVO DEL LADO DEL SERVIDOR EL CUAL CONTIENE LAS CONEXIONES A LAS BASES DE DATOS Y LAS CONSULTAL
  var urlBase = 'model/modulos/modulo1';

  //EJECUCION DE SERVICIOS
  this.getData = function () {
    return $http.get(urlBase + "/get-data");
  };

  this.agregarNuevo = function(d){
    return $http.post(urlBase + "/agregar-nuevo", d);
  }

  /*this.desactivar = function(d){
    return $http.post(urlBase + "/desactivar", d);
  }

  this.revisarVersion = function(d){
    return $http.post(urlBase + "/revisar-version", d);
  }

  this.getPlataforma = function(){
    return $http.get(urlBase + "/get-plataforma");
  }*/

}]);
